class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :update, :destroy]

  # GET /students
  def index
    sort_by = params[:sort_by]
    search_by = params[:search_by] 
    @students = if sort_by
      column = sort_by.keys.first
      order = sort_by.values.first
      Student.order(column => order)  
    elsif search_by
      column = search_by.keys.first # Can be used to search by different columns
      value = search_by.values.first
      Student.where(id: value)
    else
      Student.all
    end

    render json: @students
  end

  # GET /students/1
  def show
    render json: @student.as_json(only: [:id, :name, :age, :division, :gender])
  end

  # POST /students
  def create
    @student = Student.new(student_params)

    if @student.save
      render json: @student, status: :created, location: @student
    else
      puts @student.errors.full_messages
      render json: @student.errors.full_messages, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /students/1
  def update
    if @student.update(student_params)
      render json: @student
    else
      render json: @student.errors, status: :unprocessable_entity
    end
  end

  # DELETE /students/1
  def destroy
    @student.destroy

    render json: @student, status: 200
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find_by_id(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def student_params
      params.require(:student).permit(:name, :age, :gender, :division)
    end
end
