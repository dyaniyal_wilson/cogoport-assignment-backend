class Student < ApplicationRecord

  validates :name, 
    presence: true, 
    format: { with: /\A^[a-zA-Z_ ]*$\z/, message: "only allows letters." }

  validates :age, 
    numericality: true, 
    presence: true

  validates :gender, 
    presence: true, 
    inclusion: { in: %w(Male Female),
    message: "Please select valid %{value}." }

  validates :division, 
    presence: true, 
    inclusion: { in: %w(A B C D E F),
    message: "Please select valid %{value}." }

end
